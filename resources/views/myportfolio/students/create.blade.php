@extends('main')

@section('title', 'Decki\'s Portfolio - Students')

@section('section')
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1 class="mt-3">Student Form</h1>
                <form method="post" action="/myportfolio/students">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" autofocus>
                    </div>
                    <div class="form-group">
                        <label for="nrp">NRP</label>
                        <input type="text" class="form-control" id="nrp" name="nrp" placeholder="NRP">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="majors">Majors</label>
                        <input type="text" class="form-control" id="majors" name="majors" placeholder="Majors">
                    </div>
                    <a href="/myportfolio/students" class="btn btn-secondary float-right ml-1">Cancel</a>
                    <button type="submit" class="btn btn-primary float-right">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection